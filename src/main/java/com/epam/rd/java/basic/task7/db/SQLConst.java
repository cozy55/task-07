package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SQLConst {
	public static final String GET_ALL_USERS = "SELECT * FROM users ORDER BY id";
	public static final String GET_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
	public static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
	public static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
	public static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)";
	public static final String GER_TEAMS_FOR_USER = "SELECT id, name " +
			"FROM users_teams as ut LEFT JOIN teams as t ON ut.team_id = t.id WHERE user_id = ?";
	public static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	public static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";
	public static final String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name = ?";
	public static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	public static final String DELETE_USER_BY_NAME = "DELETE FROM users WHERE login = ?";
}

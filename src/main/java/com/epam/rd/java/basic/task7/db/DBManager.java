package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USER = "root";
	private static final String PASSWORD = "q623891q";
/*	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}
		return instance;
	}
*/
	private static final String APP_PROPERTIES = "app.properties";
	private static final String CONNECTION_URL = "connection.url";
	private static final Logger logger = Logger.getLogger(Exception.class.getName());;

	private static DBManager instance;
	private static String url;

	private DBManager() {}

	public static synchronized DBManager getInstance() {
		if(instance == null){
			url = getURL();
			instance = new DBManager();
		}
		return instance;
	}

	public static String getURL() {
		if (url == null) {
			var prop = new Properties();
			try (InputStream inputStream = new FileInputStream(APP_PROPERTIES)) {
				prop.load(inputStream);
			} catch (IOException e) {
				logger.log(Level.SEVERE,e.getMessage());
			}
			url = prop.getProperty(CONNECTION_URL);
		}
		return url;
	}


	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(url);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SQLConst.GET_ALL_USERS);){

			while (rs.next()){
				User user = User.createUser(rs.getString("login"));
				user.setId(rs.getInt("id"));
				users.add(user);
			}
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return users;
	}
/*
	public boolean insertUser(User user) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
				PreparedStatement stmt = con.prepareStatement(SQLConst.INSERT_USER, Statement.RETURN_GENERATED_KEYS);){
			stmt.setString(1, user.getLogin());
			int count = stmt.executeUpdate();
			if(count > 0) {
				try(ResultSet rs = stmt.getGeneratedKeys()) {
					if(rs.next()) {
						user.setId(rs.getInt(1));
						return true;
					}
				}
			}
			return false;
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}
*/
	public boolean insertUser(User user) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SQLConst.INSERT_USER,Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1,user.getLogin());
			statement.execute();
			var resultSet = statement.getGeneratedKeys();
			if (resultSet.next()){
				user.setId(resultSet.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(url);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			con.setAutoCommit(false);
			for(User user : users){
				stmt = con.prepareStatement(SQLConst.DELETE_USER_BY_NAME);
				stmt.setString(1, user.getLogin());
				int count = stmt.executeUpdate();
				if(count == 0) {
					return false;
				}
			}
			con.commit();
			return true;
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}finally {
			close(con);
		}
	}

	public User getUser(String login) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
				PreparedStatement stmt = con.prepareStatement(SQLConst.GET_USER_BY_LOGIN);){
			stmt.setString(1, login);
			try (ResultSet rs = stmt.executeQuery();){
				if (rs.next()) {
					User user = User.createUser(rs.getString("login"));
					user.setId(rs.getInt("id"));
					return user;
				}
			}
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
			PreparedStatement stmt = con.prepareStatement(SQLConst.GET_TEAM_BY_NAME);){
			stmt.setString(1, name);
			try (ResultSet rs = stmt.executeQuery();){
				if (rs.next()) {
					Team team = Team.createTeam(rs.getString("name"));
					team.setId(rs.getInt("id"));
					return team;
				}
			}
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(url);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(SQLConst.GET_ALL_TEAMS);){

			while (rs.next()){
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
			PreparedStatement stmt = con.prepareStatement(SQLConst.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);){
			stmt.setString(1, team.getName());
			int count = stmt.executeUpdate();
			if(count > 0){
				try(ResultSet rs = stmt.getGeneratedKeys()) {
					if(rs.next()){
						team.setId(rs.getInt(1));
						return true;
					}
				}
			}
			return false;
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(url);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			for(Team t : teams){
				stmt = con.prepareStatement(SQLConst.INSERT_TEAMS_FOR_USER);
				stmt.setInt(1, user.getId());
				stmt.setInt(2, t.getId());
				stmt.execute();
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		} finally {
			close(con);
			close(stmt);
		}
		return false;	
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(url);
				PreparedStatement stmt = con.prepareStatement(SQLConst.GER_TEAMS_FOR_USER)){
			stmt.setInt(1, user.getId());
			try(ResultSet rs = stmt.executeQuery()){
				while (rs.next()){
					Team team = Team.createTeam(rs.getString("name"));
					team.setId(rs.getInt("id"));
					teams.add(team);
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
			PreparedStatement stmt = con.prepareStatement(SQLConst.DELETE_TEAM_BY_NAME);){
			stmt.setString(1, team.getName());
			return stmt.execute();
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try(Connection con = DriverManager.getConnection(url);
			PreparedStatement stmt = con.prepareStatement(SQLConst.UPDATE_TEAM);){
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			return stmt.execute();
		}catch (SQLException e) {
			//logged
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	private void close(AutoCloseable autoCloseable) {
		if(autoCloseable != null) {
			try {
				autoCloseable.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
